package org.polytechtours.performance.tp.fourmispeintre;

public class MatriceConv25 extends MatriceConv {

    public MatriceConv25() {

        // Allocation mémoire de la matrice
        mMatriceConv = new float[5][5];

        // initialisation de la matrice de convolution : lissage moyen sur 25
        // cases
        /*
         * 1 1 2 1 1 1 2 3 2 1 2 3 4 3 2 1 2 3 2 1 1 1 2 1 1
         */
        mMatriceConv[0][0] = 1 / 44f;
        mMatriceConv[0][1] = 1 / 44f;
        mMatriceConv[0][2] = 2 / 44f;
        mMatriceConv[0][3] = 1 / 44f;
        mMatriceConv[0][4] = 1 / 44f;
        mMatriceConv[1][0] = 1 / 44f;
        mMatriceConv[1][1] = 2 / 44f;
        mMatriceConv[1][2] = 3 / 44f;
        mMatriceConv[1][3] = 2 / 44f;
        mMatriceConv[1][4] = 1 / 44f;
        mMatriceConv[2][0] = 2 / 44f;
        mMatriceConv[2][1] = 3 / 44f;
        mMatriceConv[2][2] = 4 / 44f;
        mMatriceConv[2][3] = 3 / 44f;
        mMatriceConv[2][4] = 2 / 44f;
        mMatriceConv[3][0] = 1 / 44f;
        mMatriceConv[3][1] = 2 / 44f;
        mMatriceConv[3][2] = 3 / 44f;
        mMatriceConv[3][3] = 2 / 44f;
        mMatriceConv[3][4] = 1 / 44f;
        mMatriceConv[4][0] = 1 / 44f;
        mMatriceConv[4][1] = 1 / 44f;
        mMatriceConv[4][2] = 2 / 44f;
        mMatriceConv[4][3] = 1 / 44f;
        mMatriceConv[4][4] = 1 / 44f;
    }
}
