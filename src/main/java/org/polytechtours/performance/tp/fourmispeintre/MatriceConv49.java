package org.polytechtours.performance.tp.fourmispeintre;

public class MatriceConv49 extends MatriceConv {

    public MatriceConv49() {

        // Allocation mémoire de la matrice
        mMatriceConv = new float[7][7];

        // initialisation de la matrice de convolution : lissage moyen sur 49
        // cases
        /*
         * 1 1 2 2 2 1 1 1 2 3 4 3 2 1 2 3 4 5 4 3 2 2 4 5 8 5 4 2 2 3 4 5 4 3 2 1 2
         * 3 4 3 2 1 1 1 2 2 2 1 1
         */
        mMatriceConv[0][0] = 1 / 128f;
        mMatriceConv[0][1] = 1 / 128f;
        mMatriceConv[0][2] = 2 / 128f;
        mMatriceConv[0][3] = 2 / 128f;
        mMatriceConv[0][4] = 2 / 128f;
        mMatriceConv[0][5] = 1 / 128f;
        mMatriceConv[0][6] = 1 / 128f;

        mMatriceConv[1][0] = 1 / 128f;
        mMatriceConv[1][1] = 2 / 128f;
        mMatriceConv[1][2] = 3 / 128f;
        mMatriceConv[1][3] = 4 / 128f;
        mMatriceConv[1][4] = 3 / 128f;
        mMatriceConv[1][5] = 2 / 128f;
        mMatriceConv[1][6] = 1 / 128f;

        mMatriceConv[2][0] = 2 / 128f;
        mMatriceConv[2][1] = 3 / 128f;
        mMatriceConv[2][2] = 4 / 128f;
        mMatriceConv[2][3] = 5 / 128f;
        mMatriceConv[2][4] = 4 / 128f;
        mMatriceConv[2][5] = 3 / 128f;
        mMatriceConv[2][6] = 2 / 128f;

        mMatriceConv[3][0] = 2 / 128f;
        mMatriceConv[3][1] = 4 / 128f;
        mMatriceConv[3][2] = 5 / 128f;
        mMatriceConv[3][3] = 8 / 128f;
        mMatriceConv[3][4] = 5 / 128f;
        mMatriceConv[3][5] = 4 / 128f;
        mMatriceConv[3][6] = 2 / 128f;

        mMatriceConv[4][0] = 2 / 128f;
        mMatriceConv[4][1] = 3 / 128f;
        mMatriceConv[4][2] = 4 / 128f;
        mMatriceConv[4][3] = 5 / 128f;
        mMatriceConv[4][4] = 4 / 128f;
        mMatriceConv[4][5] = 3 / 128f;
        mMatriceConv[4][6] = 2 / 128f;

        mMatriceConv[5][0] = 1 / 128f;
        mMatriceConv[5][1] = 2 / 128f;
        mMatriceConv[5][2] = 3 / 128f;
        mMatriceConv[5][3] = 4 / 128f;
        mMatriceConv[5][4] = 3 / 128f;
        mMatriceConv[5][5] = 2 / 128f;
        mMatriceConv[5][6] = 1 / 128f;

        mMatriceConv[6][0] = 1 / 128f;
        mMatriceConv[6][1] = 1 / 128f;
        mMatriceConv[6][2] = 2 / 128f;
        mMatriceConv[6][3] = 2 / 128f;
        mMatriceConv[6][4] = 2 / 128f;
        mMatriceConv[6][5] = 1 / 128f;
        mMatriceConv[6][6] = 1 / 128f;
    }
}
