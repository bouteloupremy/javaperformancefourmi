package org.polytechtours.performance.tp.fourmispeintre;
// package PaintingAnts_v2;

import javax.swing.*;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;

// version : 2.0

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

/**
 * <p>
 * Titre : Painting Ants
 * </p>
 * <p>
 * Description :
 * </p>
 * <p>
 * Copyright : Copyright (c) 2003
 * </p>
 * <p>
 * Société : Equipe Réseaux/TIC - Laboratoire d'Informatique de l'Université de
 * Tours
 * </p>
 *
 * @author Nicolas Monmarché
 * @version 1.0
 */

public class CPainting extends JPanel implements MouseListener {
  private static final long serialVersionUID = 1L;
  // Objet de type Graphics permettant de manipuler l'affichage du Canvas
  private Graphics mGraphics;

  // Objet ne servant que pour les bloc synchronized pour la manipulation du
  // tableau des couleurs
  private Object mMutexCouleurs = new Object();

  private int[] mCouleurs;

  // tableau des couleurs, il permert de conserver en memoire l'état de chaque
  // pixel du canvas, ce qui est necessaire au deplacemet des fourmi
  // il sert aussi pour la fonction paint du Canvas
  private BufferedImage mBaseImagee;

  // couleur du fond
  private Color mCouleurFond = new Color(255, 255, 255);

  // dimensions
  private Dimension mDimension = new Dimension();

  private PaintingAnts mApplis;

  private boolean mSuspendu = false;

  /******************************************************************************
   * Titre : public CPainting() Description : Constructeur de la classe
   ******************************************************************************/
  public CPainting(Dimension pDimension, PaintingAnts pApplis) {
    int i, j;
    addMouseListener(this);

    mApplis = pApplis;

    mDimension = pDimension;
    setBounds(new Rectangle(0, 0, mDimension.width, mDimension.height));

    this.setBackground(mCouleurFond);

    // Initialisation de la matrice couleur
    mCouleurs = new int[mDimension.height*mDimension.width];

    mBaseImagee = new BufferedImage(mDimension.width,mDimension.height, BufferedImage.TYPE_INT_RGB);
    resize(mDimension);

    synchronized (mMutexCouleurs) {
      int couleurFond = mCouleurFond.getRGB();

      // initialisation de la matrice des couleurs
      for (i = 0; i != mDimension.width; i++)
        for (j = 0; j != mDimension.height; j++)
          mCouleurs[i*mDimension.width + j] = couleurFond;
    }
  }

  /******************************************************************************
   * Titre : Color getCouleur Description : Cette fonction renvoie la couleur
   * d'une case
   ******************************************************************************/
  public int getCouleur(int x, int y) {
      return mCouleurs[x*mDimension.width + y];
  }

  /******************************************************************************
   * Titre : Color getDimension Description : Cette fonction renvoie la
   * dimension de la peinture
   ******************************************************************************/
  public Dimension getDimension() {
    return mDimension;
  }

  /******************************************************************************
   * Titre : Color getHauteur Description : Cette fonction renvoie la hauteur de
   * la peinture
   ******************************************************************************/
  public int getHauteur() {
    return mDimension.height;
  }

  /******************************************************************************
   * Titre : Color getLargeur Description : Cette fonction renvoie la hauteur de
   * la peinture
   ******************************************************************************/
  public int getLargeur() {
    return mDimension.width;
  }

  /******************************************************************************
   * Titre : void init() Description : Initialise le fond a la couleur blanche
   * et initialise le tableau des couleurs avec la couleur blanche
   ******************************************************************************/
  public void init() {
    int i, j;
    mGraphics = getGraphics();

    synchronized (mMutexCouleurs) {
      int couleurFond = mCouleurFond.getRGB();

      mGraphics.clearRect(0, 0, mDimension.width, mDimension.height);

      // initialisation de la matrice des couleurs
      for (i = 0; i != mDimension.width; i++)
        for (j = 0; j != mDimension.height; j++)
          mCouleurs[i*mDimension.width + j] = couleurFond;
    }

    mSuspendu = false;
  }

  /****************************************************************************/
  public void mouseClicked(MouseEvent pMouseEvent) {
    pMouseEvent.consume();
    if (pMouseEvent.getButton() == MouseEvent.BUTTON1) {
      // double clic sur le bouton gauche = effacer et recommencer
      if (pMouseEvent.getClickCount() == 2) {
        init();
      }
      // simple clic = suspendre les calculs et l'affichage
      mApplis.pause();
    } else {
      // bouton du milieu (roulette) = suspendre l'affichage mais
      // continuer les calculs
      if (pMouseEvent.getButton() == MouseEvent.BUTTON2) {
        suspendre();
      } else {
        // clic bouton droit = effacer et recommencer
        // case pMouseEvent.BUTTON3:
        init();
      }
    }
  }

  /****************************************************************************/
  public void mouseEntered(MouseEvent pMouseEvent) {
  }

  /****************************************************************************/
  public void mouseExited(MouseEvent pMouseEvent) {
  }

  /****************************************************************************/
  public void mousePressed(MouseEvent pMouseEvent) {

  }

  /****************************************************************************/
  public void mouseReleased(MouseEvent pMouseEvent) {
  }

  /******************************************************************************
   * Titre : void paint(Graphics g) Description : Surcharge de la fonction qui
   * est appelé lorsque le composant doit être redessiné
   ******************************************************************************/
  @Override
  public void paint(Graphics pGraphics) {
    mBaseImagee.getRaster().setDataElements(0,0,mDimension.width,mDimension.height, mCouleurs);
    pGraphics.drawImage(mBaseImagee, 0, 0, mDimension.width, mDimension.height,this);
  }

  @Override
  public void paintComponent(Graphics pGraphics) {
      super.paintComponent(pGraphics);
  }

  /******************************************************************************
   * Titre : void colorer_case(int x, int y, Color c) Description : Cette
   * fonction va colorer le pixel correspondant et mettre a jour le tableau des
   * couleurs
   ******************************************************************************/
  public void setCouleur(int x, int y, int c, int pTaille) {
    int i, j, k, l, m, n, nbCase = 0, nbCaseMoins1 = 0;

    MatriceConv matriceConv = new MatriceConv9();

    float R, G, B;
    int lColor;

    synchronized (mMutexCouleurs) {
        if (!mSuspendu) {
            // on colorie la case sur laquelle se trouve la fourmi
            mCouleurs[x * mDimension.width + y] = c;
        }

        // on fait diffuser la couleur :
        switch (pTaille) {
            case 1:
                nbCase = 3;
                matriceConv = new MatriceConv9();
                break;
            case 2:
                nbCase = 5;
                matriceConv = new MatriceConv25();
                break;
            case 3:
                nbCase = 7;
                matriceConv = new MatriceConv49();
                break;
        }

        if (pTaille == 1 || pTaille == 2 || pTaille == 3) {

            // produit de convolution discrete sur 9 cases
            for (i = 0; i < nbCase; i++) {
                for (j = 0; j < nbCase; j++) {
                    R = G = B = 0f;

                    for (k = 0; k < nbCase; k++) {
                        for (l = 0; l < nbCase; l++) {
                            m = (x + i + k - (nbCase - 1) + mDimension.width) % mDimension.width;
                            n = (y + j + l - (nbCase - 1) + mDimension.height) % mDimension.height;
                            R += matriceConv.getmMatriceConv()[k][l] * ((mCouleurs[m * mDimension.width + n]>> 16) & 0xFF);
                            G += matriceConv.getmMatriceConv()[k][l] * ((mCouleurs[m * mDimension.width + n]>> 8) & 0xFF);
                            B += matriceConv.getmMatriceConv()[k][l] * (mCouleurs[m * mDimension.width + n] & 0xFF);
                        }
                    }

                    lColor = 65536 * ((int) R) + 256 * ((int)G) + (int)B;

                    m = (x + i - ((nbCase - 1) / 2) + mDimension.width) % mDimension.width;
                    n = (y + j - ((nbCase - 1) / 2) + mDimension.height) % mDimension.height;

                    if (!mSuspendu)
                        mCouleurs[m * mDimension.width + n] = lColor;
                }
            }
        }
    }
  }

  /******************************************************************************
   * Titre : setSupendu Description : Cette fonction change l'état de suspension
   ******************************************************************************/

  public void suspendre() {
    mSuspendu = !mSuspendu;
    if (!mSuspendu) {
      repaint();
    }
  }
}
