package org.polytechtours.performance.tp.fourmispeintre;

public class MatriceConv9 extends MatriceConv {

    public MatriceConv9() {

        // Allocation mémoire de la matrice
        mMatriceConv = new float[3][3];

        // initialisation de la matrice de convolution : lissage moyen sur 9
        // cases
        /*
         * 1 2 1 2 4 2 1 2 1
         */
        mMatriceConv[0][0] = 1 / 16f;
        mMatriceConv[0][1] = 2 / 16f;
        mMatriceConv[0][2] = 1 / 16f;
        mMatriceConv[1][0] = 2 / 16f;
        mMatriceConv[1][1] = 4 / 16f;
        mMatriceConv[1][2] = 2 / 16f;
        mMatriceConv[2][0] = 1 / 16f;
        mMatriceConv[2][1] = 2 / 16f;
        mMatriceConv[2][2] = 1 / 16f;
    }
}
