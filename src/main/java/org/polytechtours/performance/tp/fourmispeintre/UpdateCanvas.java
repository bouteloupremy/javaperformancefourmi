package org.polytechtours.performance.tp.fourmispeintre;

import java.awt.*;

import static java.lang.Thread.sleep;

public class UpdateCanvas implements Runnable {

    private PaintingAnts paintingAnts;

    public UpdateCanvas(PaintingAnts paintingAnts) {
        this.paintingAnts = paintingAnts;
    }

    @Override
    public void run() {

        try {
            while(true) {
                sleep(100);
                paintingAnts.mPainting.repaint();
            }
        } catch (InterruptedException e) {
            System.out.println("Impossible de mettre à jour le canvas" + e.getMessage());
        }
    }
}
